//
//  main.m
//  OhMyDairy
//
//  Created by Gowtham Namuri on 28/09/16.
//  Copyright © 2016 Gowtham Namuri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
